**1. Install Dark Theme**
 - Install One Dark Theme
 - Install theme Darcula Dark
 - Import editor style color Scheme: file from git at: **Files->IntelliJ IDE->Darcula_Darker_vncud.icls**
 - Install Atom Icon
 - Or login and sync Mobi account

**2. Set color for active Tab in editor:** Editor -> Color Scheme -> General -> Editor -> Tab -> Underline
**3. Nice plugins:**
- [Prettier](https://www.jetbrains.com/help/idea/prettier.html#ws_prettier_reformat_code)
- [Translation](https://plugins.jetbrains.com/plugin/8579-translation)

**4. Format code with Prettier**

![alt text](/Files/assets/prettier-setup.png)

